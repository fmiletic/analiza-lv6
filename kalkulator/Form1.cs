﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace kalkulator
{
	public partial class Form1 : Form
	{
		double a, b, rez;
		public Form1()
		{
			InitializeComponent();
		}

		private void tA_TextChanged(object sender, EventArgs e)
		{
			double.TryParse(tA.Text,out a);
		}

		private void bRazlika_Click(object sender, EventArgs e)
		{
			if (tA.Text == "" || tB.Text == "")
			{
				MessageBox.Show("Unesite a i b");
			}
			else
			{
				label3.Text = "a-b";
				lRezultat.Text = (a - b).ToString();
			}
		}

		private void bUmnozak_Click(object sender, EventArgs e)
		{
			if (tA.Text == "" || tB.Text == "")
			{
				MessageBox.Show("Unesite a i b");
			}
			else
			{
				label3.Text = "a*b";
				lRezultat.Text = (a * b).ToString();
			}
		}

		private void bKolicnik_Click(object sender, EventArgs e)
		{
			if (tA.Text == "" || tB.Text == "")
			{
				MessageBox.Show("Unesite a i b");
			}
			else
			{
				if (tB.Text == "0")
				{
					MessageBox.Show("Nazivnik ne smije biti 0.");
				}
				else
				{
					label3.Text = "a/b";
					lRezultat.Text = (a / b).ToString();
				}
			}
		}

		private void bSin_Click(object sender, EventArgs e)
		{
			if(tA.Text=="" && tB.Text == "")
			{
				MessageBox.Show("Unesite a ili b");
			}
			else if (tA.Text != "" && tB.Text!="")
			{
				MessageBox.Show("Izbrišite a ili b");
			}
			else if (tA.Text != "")
			{
				label3.Text = "sin(a)";
				lRezultat.Text = (Math.Sin(a)).ToString();
			}
			else if (tB.Text != "")
			{
				label3.Text = "sin(b)";
				lRezultat.Text = (Math.Sin(b)).ToString();
			}
		}

		private void bCos_Click(object sender, EventArgs e)
		{
			if (tA.Text == "" && tB.Text == "")
			{
				MessageBox.Show("Unesite a ili b");
			}
			else if (tA.Text != "" && tB.Text != "")
			{
				MessageBox.Show("Izbrišite a ili b");
			}
			else if (tA.Text != "")
			{
				label3.Text = "cos(a)";
				lRezultat.Text = (Math.Cos(a)).ToString();
			}
			else if (tB.Text != "")
			{
				label3.Text = "cos(b)";
				lRezultat.Text = (Math.Cos(b)).ToString();
			}
		}

		private void bLog_Click(object sender, EventArgs e)
		{
			if (tA.Text == "" && tB.Text == "")
			{
				MessageBox.Show("Unesite a ili b");
			}
			else if (tA.Text != "" && tB.Text != "")
			{
				MessageBox.Show("Izbrišite a ili b");
			}
			else if (tA.Text != "")
			{
				if (a <= 0)
				{
					MessageBox.Show("Broj mora biti pozitivan");
				}
				else
				{
					label3.Text = "log(a)";
					lRezultat.Text = (Math.Log(a)).ToString();
				}
			}
			else if (tB.Text != "")
			{
				if (b <= 0)
				{
					MessageBox.Show("Broj mora biti pozitivan");
				}
				label3.Text = "log(b)";
				lRezultat.Text = (Math.Log(b)).ToString();
			}
		}

		private void bSqrt_Click(object sender, EventArgs e)
		{
			if (tA.Text == "" && tB.Text == "")
			{
				MessageBox.Show("Unesite a ili b");
			}
			else if (tA.Text != "" && tB.Text != "")
			{
				MessageBox.Show("Izbrišite a ili b");
			}
			else if (tA.Text != "")
			{
				if (a < 0)
				{
					MessageBox.Show("Broj mora biti pozitivan");
				}
				else
				{
					label3.Text = "sqrt(a)";
					lRezultat.Text = (Math.Sqrt(a)).ToString();
				}
			}
			else if (tB.Text != "")
			{
				if (b < 0)
				{
					MessageBox.Show("Broj mora biti pozitivan");
				}
				label3.Text = "sqrt(b)";
				lRezultat.Text = (Math.Sqrt(b)).ToString();
			}
		}

		private void bPow_Click(object sender, EventArgs e)
		{
			if (tA.Text == "" || tB.Text == "")
			{
				MessageBox.Show("Unesite a i b");
			}
			else
			{
				label3.Text = "pow(a,b)";
				lRezultat.Text = (Math.Pow(a,b)).ToString();
			}
		}

		private void tB_TextChanged(object sender, EventArgs e)
		{
			double.TryParse(tB.Text, out b);
		}

		private void bZbroj_Click(object sender, EventArgs e)
		{
			if(tA.Text=="" || tB.Text == "")
			{
				MessageBox.Show("Unesite a i b");
			}
			else
			{
				label3.Text = "a+b";
				lRezultat.Text = (a + b).ToString();
			}
		}
	}
}
