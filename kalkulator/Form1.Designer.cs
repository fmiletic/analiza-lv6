﻿namespace kalkulator
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.tA = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.tB = new System.Windows.Forms.TextBox();
			this.bZbroj = new System.Windows.Forms.Button();
			this.bRazlika = new System.Windows.Forms.Button();
			this.bUmnozak = new System.Windows.Forms.Button();
			this.bKolicnik = new System.Windows.Forms.Button();
			this.bSin = new System.Windows.Forms.Button();
			this.bCos = new System.Windows.Forms.Button();
			this.bLog = new System.Windows.Forms.Button();
			this.bSqrt = new System.Windows.Forms.Button();
			this.bPow = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.lRezultat = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(93, 33);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(24, 17);
			this.label1.TabIndex = 0;
			this.label1.Text = "a=";
			// 
			// tA
			// 
			this.tA.Location = new System.Drawing.Point(123, 30);
			this.tA.Name = "tA";
			this.tA.Size = new System.Drawing.Size(100, 22);
			this.tA.TabIndex = 1;
			this.tA.TextChanged += new System.EventHandler(this.tA_TextChanged);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(93, 71);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(24, 17);
			this.label2.TabIndex = 2;
			this.label2.Text = "b=";
			// 
			// tB
			// 
			this.tB.Location = new System.Drawing.Point(123, 68);
			this.tB.Name = "tB";
			this.tB.Size = new System.Drawing.Size(100, 22);
			this.tB.TabIndex = 3;
			this.tB.TextChanged += new System.EventHandler(this.tB_TextChanged);
			// 
			// bZbroj
			// 
			this.bZbroj.Location = new System.Drawing.Point(78, 114);
			this.bZbroj.Name = "bZbroj";
			this.bZbroj.Size = new System.Drawing.Size(41, 37);
			this.bZbroj.TabIndex = 4;
			this.bZbroj.Text = "+";
			this.bZbroj.UseVisualStyleBackColor = true;
			this.bZbroj.Click += new System.EventHandler(this.bZbroj_Click);
			// 
			// bRazlika
			// 
			this.bRazlika.Location = new System.Drawing.Point(125, 114);
			this.bRazlika.Name = "bRazlika";
			this.bRazlika.Size = new System.Drawing.Size(41, 36);
			this.bRazlika.TabIndex = 6;
			this.bRazlika.Text = "-";
			this.bRazlika.UseVisualStyleBackColor = true;
			this.bRazlika.Click += new System.EventHandler(this.bRazlika_Click);
			// 
			// bUmnozak
			// 
			this.bUmnozak.Location = new System.Drawing.Point(172, 114);
			this.bUmnozak.Name = "bUmnozak";
			this.bUmnozak.Size = new System.Drawing.Size(41, 36);
			this.bUmnozak.TabIndex = 7;
			this.bUmnozak.Text = "*";
			this.bUmnozak.UseVisualStyleBackColor = true;
			this.bUmnozak.Click += new System.EventHandler(this.bUmnozak_Click);
			// 
			// bKolicnik
			// 
			this.bKolicnik.Location = new System.Drawing.Point(219, 114);
			this.bKolicnik.Name = "bKolicnik";
			this.bKolicnik.Size = new System.Drawing.Size(41, 37);
			this.bKolicnik.TabIndex = 8;
			this.bKolicnik.Text = "/";
			this.bKolicnik.UseVisualStyleBackColor = true;
			this.bKolicnik.Click += new System.EventHandler(this.bKolicnik_Click);
			// 
			// bSin
			// 
			this.bSin.Location = new System.Drawing.Point(87, 157);
			this.bSin.Name = "bSin";
			this.bSin.Size = new System.Drawing.Size(52, 37);
			this.bSin.TabIndex = 9;
			this.bSin.Text = "sin";
			this.bSin.UseVisualStyleBackColor = true;
			this.bSin.Click += new System.EventHandler(this.bSin_Click);
			// 
			// bCos
			// 
			this.bCos.Location = new System.Drawing.Point(145, 157);
			this.bCos.Name = "bCos";
			this.bCos.Size = new System.Drawing.Size(52, 37);
			this.bCos.TabIndex = 10;
			this.bCos.Text = "cos";
			this.bCos.UseVisualStyleBackColor = true;
			this.bCos.Click += new System.EventHandler(this.bCos_Click);
			// 
			// bLog
			// 
			this.bLog.Location = new System.Drawing.Point(203, 157);
			this.bLog.Name = "bLog";
			this.bLog.Size = new System.Drawing.Size(52, 37);
			this.bLog.TabIndex = 11;
			this.bLog.Text = "log";
			this.bLog.UseVisualStyleBackColor = true;
			this.bLog.Click += new System.EventHandler(this.bLog_Click);
			// 
			// bSqrt
			// 
			this.bSqrt.Location = new System.Drawing.Point(170, 200);
			this.bSqrt.Name = "bSqrt";
			this.bSqrt.Size = new System.Drawing.Size(52, 37);
			this.bSqrt.TabIndex = 12;
			this.bSqrt.Text = "sqrt";
			this.bSqrt.UseVisualStyleBackColor = true;
			this.bSqrt.Click += new System.EventHandler(this.bSqrt_Click);
			// 
			// bPow
			// 
			this.bPow.Location = new System.Drawing.Point(112, 200);
			this.bPow.Name = "bPow";
			this.bPow.Size = new System.Drawing.Size(52, 37);
			this.bPow.TabIndex = 13;
			this.bPow.Text = "pow";
			this.bPow.UseVisualStyleBackColor = true;
			this.bPow.Click += new System.EventHandler(this.bPow_Click);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(93, 258);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(64, 17);
			this.label3.TabIndex = 14;
			this.label3.Text = "Rezultat:";
			// 
			// lRezultat
			// 
			this.lRezultat.AutoSize = true;
			this.lRezultat.Location = new System.Drawing.Point(177, 258);
			this.lRezultat.Name = "lRezultat";
			this.lRezultat.Size = new System.Drawing.Size(0, 17);
			this.lRezultat.TabIndex = 15;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.ClientSize = new System.Drawing.Size(341, 319);
			this.Controls.Add(this.lRezultat);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.bPow);
			this.Controls.Add(this.bSqrt);
			this.Controls.Add(this.bLog);
			this.Controls.Add(this.bCos);
			this.Controls.Add(this.bSin);
			this.Controls.Add(this.bKolicnik);
			this.Controls.Add(this.bUmnozak);
			this.Controls.Add(this.bRazlika);
			this.Controls.Add(this.bZbroj);
			this.Controls.Add(this.tB);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.tA);
			this.Controls.Add(this.label1);
			this.Name = "Form1";
			this.Text = "Kalkulator";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox tA;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox tB;
		private System.Windows.Forms.Button bZbroj;
		private System.Windows.Forms.Button bRazlika;
		private System.Windows.Forms.Button bUmnozak;
		private System.Windows.Forms.Button bKolicnik;
		private System.Windows.Forms.Button bSin;
		private System.Windows.Forms.Button bCos;
		private System.Windows.Forms.Button bLog;
		private System.Windows.Forms.Button bSqrt;
		private System.Windows.Forms.Button bPow;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label lRezultat;
	}
}

